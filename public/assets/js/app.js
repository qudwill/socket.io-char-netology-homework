'use strict';

$(function() {
  var $window = $(window);
  var $usernameInput = $('.usernameInput');
  var $messages = $('.messages');
  var $inputMessage = $('.inputMessage');
  var $loginPage = $('.login.page');
  var $chatPage = $('.chat.page');
  var username;
  var room;
  var connected = false;
  var socket = io();

  function cleanInput(input) {
    return $('<div/>').text(input).html();
  }

  function setUsername() {
    username = cleanInput($usernameInput.val().trim());
    room = cleanInput($('#room').val());

    if (username) {
      $loginPage.fadeOut();
      $chatPage.show();

      socket.emit('add user', {
        username: username,
        room: room
      });
    }
  }

  function sendMessage() {
    var message = cleanInput($inputMessage.val());

    if (message && connected) {
      $inputMessage.val('');

      addChatMessage({
        username: username,
        message: message,
        room: room
      });

      socket.emit('new message', message);
    }
  }

  function log(data) {
    if (room === data.room) {
      var $el = $('<li>').addClass('log').text(data.message);

      addMessageElement($el);
    }
  }

  function addChatMessage(data) {
    if (room === data.room) {
      var $usernameDiv = $('<span class="username">').text(data.username);
      var $messageBodyDiv = $('<span class="messageBody">').text(data.message);
      var $messageDiv = $('<li class="message">').append($usernameDiv, $messageBodyDiv);
  
      addMessageElement($messageDiv);
    }
  }

  function addMessageElement(el) {
    var $el = $(el);

    $messages.append($el);
  }

  $window.keydown(function(e) {
    if (e.which === 13) {
      username ? sendMessage() : setUsername();
    }
  });

  socket.on('login', function(data) {
    connected = true;
  
    var message = 'Welcome to Socket.Io Chat - ROOM #' + data.room;

    log({
      message: message,
      room: data.room
    });
  });

  socket.on('new message', function(data) {
    addChatMessage(data);
  });

  socket.on('user joined', function(data) {
    log({
      message: data.username + ' joined to the room #' + data.room,
      room: data.room
    });
  });
});