'use strict';

var express= require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

http.listen(3000, () => {
	console.log('Server listening at port %d', port);
});

app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', socket => {
	socket.on('new message', data => {
		socket.broadcast.emit('new message', {
			username: socket.username,
			room: socket.room,
			message: data
		});
	});

	socket.on('add user', data => {
		socket.username = data.username;
		socket.room = data.room;

		socket.emit('login', {
			username: socket.username,
			room: socket.room
		});

		socket.broadcast.emit('user joined', {
			username: socket.username,
			room: socket.room
		});
	});
});